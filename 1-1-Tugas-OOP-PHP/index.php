<?php
abstract class hewan {
    public $nama, $darah = 50, $jumlahKaki, $keahlian;

    public function __construct($nama){
        $this->nama = $nama;
    }

    abstract public function getInfoHewan();
    public function atraksi()
    {
        $str = $this->nama . " sedang " . $this->keahlian;
        return $str;
    }
}

trait fight{
    public $attackPower, $defencePower;
    
    public function serang($hewan)
    {
        echo "$this->nama sedang menyerang $hewan->nama" . "<br>";
        echo "================================================" . "<br>";
        $hewan->diserang($this);
    }

    public function diserang($hewan)
    {
        echo "$this->nama sedang diserang $hewan->nama" . "<br>";
        $this->darah = $this->darah -  ($hewan->attackPower / $this->defencePower);

        echo " Darah $this->nama tersisa  : $this->darah";
    }


}

class elang extends hewan {
    use fight;

    public function __construct($nama)
    {
        parent::__construct($nama);
        $this->jumlahKaki = 2 ;
        $this->keahlian = "Terbang Tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getInfoHewan(){
        $str =  "==== Elang ====" . "<br>" .
                "Nama : $this->nama" . "<br>".
                "Darah : $this->darah" . "<br>".
                "Jumlah Kaki : $this->jumlahKaki" . "<br>".
                "keahlian : $this->keahlian" . "<br>".
                "Attack Power : $this->attackPower" . "<br>".
                "Defence Power : $this->defencePower" ;
        return $str;
    }
}

class harimau extends hewan {
    use fight;

    public function __construct($nama)
    {
        parent::__construct($nama);
        $this->jumlahKaki = 4 ;
        $this->keahlian = "Lari Cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan(){
        $str =  "==== Harimau ====" . "<br>" .
                "Nama : $this->nama" . "<br>".
                "Darah : $this->darah" . "<br>".
                "Jumlah Kaki : $this->jumlahKaki" . "<br>".
                "keahlian : $this->keahlian" . "<br>".
                "Attack Power : $this->attackPower" . "<br>".
                "Defence Power : $this->defencePower" ;
        return $str;
    }

} 

$elang_1 = new elang("elang_1");
$harimau_1 = new harimau("harimau_1");
echo $elang_1->atraksi() . " <br> ";
echo $harimau_1->atraksi() . " <br>  <br>";

echo $elang_1->getInfoHewan() . " <br> <br>";
echo $harimau_1->getInfoHewan() . " <br>  <br>";

echo $elang_1->serang($harimau_1) . " <br> <br>";
echo $harimau_1->serang($elang_1) . " <br> <br>";
?>