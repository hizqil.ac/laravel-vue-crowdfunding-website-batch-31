// soal no 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

//Mengurutkan data
daftarHewan.sort();

//looping data
daftarHewan.forEach(function(data) {
    console.log(data)
})




//Soal no 2
function introduce(params) {
    return ("Nama Saya " + params.name + ", umur saya " + params.age + " tahun, alamat saya di " + params.address + ", dan saya punya hobby yaitu " + params.hobby);
}

var data = {
    name: "John",
    age: 30,
    address: "Jalan Pelesiran",
    hobby: "Gaming"
}

var perkenalan = introduce(data)
console.log(perkenalan)



//soal no 3

const hurufVocal = ["a", "i", "u", "e", "o"]

function hitung_huruf_vokal(data) {
    let count = 0;
    //Konversi huruf kapital ke huruf kecil 
    for (let kalimat of data.toLowerCase()) {
        //pengecekan huruf vocal
        if (hurufVocal.includes(kalimat)) {
            count++;
        }

    }
    return count;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Iqbal");
console.log(hitung_1, hitung_2);




//soal no 4
function hitung(value) {
    return (value * 2) - 2;
}

console.log(hitung(0)) // -2
console.log(hitung(1)) // 0
console.log(hitung(2)) // 2
console.log(hitung(3)) // 4
console.log(hitung(5)) // 8