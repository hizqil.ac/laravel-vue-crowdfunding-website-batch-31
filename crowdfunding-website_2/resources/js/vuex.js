import Vue from 'vue'
import Vuex from 'vuex'
import Vuexpersist from 'vuex-persist'

import transaction from './stores/transaction.js';
import auth from './stores/auth.js';
import alert from './stores/alert.js';

const vuexPersist = new Vuexpersist({
    key: "DonateAPP",
    storage: localStorage
})

Vue.use(Vuex)


const store = new Vuex.Store({
    plugins: [vuexPersist.plugin],
    modules: {
        transaction,
        auth,
        alert
    }
})

export default store