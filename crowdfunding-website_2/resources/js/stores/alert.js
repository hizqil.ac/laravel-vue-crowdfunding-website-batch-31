export default {
    namespaced: true,
    state: {
        alert: false,
        alertText: '',
        alertError: false
    },
    mutations: {
        getAlert: (state, payload) => {
            state.alert = true
            state.alertText = payload

        },

        getAlertError: (state, payload) => {
            state.alertError = true
            state.alertText = payload
        },

        closeAlert: (state) => {
            state.alert = false
            state.alertError = false
            state.alertText = ''
        }
    },
    actions: {

    },
    getters: {
        alert: state => state.alert,
        alertText: state => state.alertText,
        alertError: state => state.alertError,
    }
}