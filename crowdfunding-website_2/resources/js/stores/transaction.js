export default {
    namespaced: true,
    state: {
        transactions: 0
    },
    mutations: {
        insert: (state, payload) => {
            state.transactions += payload
        },
        setTransactions: (state, payload) => {
            state.transactions = payload
        }
    },
    actions: {

    },
    getters: {
        transactions: state => state.transactions
    }
}