export default {
    namespaced: true,
    state: {
        transactions: 10,
        user: {},
        Email: "",
        isLogin: false,
    },
    mutations: {
        set: (state, payload) => {
            state.user = payload
        },

        setEmail: (state, payload) => {
            state.Email = payload
        },

        setisLogin: (state, payload) => {
            state.isLogin = payload
        },

        insert: (state, payload) => {
            state.transactions += payload
        }

    },
    actions: {
        setAuth: ({ commit }, payload) => {
            commit('set', payload)
        },

        cekToken: ({ commit }, payload) => {
            axios({
                    method: "post",
                    url: "/api/Auth/Check-Token",
                    headers: {
                        Authorization: "Bearer " + payload.Data.Token,
                    },
                })
                .then((response) => {
                    commit('set', payload)

                })
                .catch((error) => {
                    commit('set', {})
                    commit('setisLogin', false)

                });


        },
    },
    getters: {
        user: state => state.user,
        isLogin: state => state.isLogin,
        guest: state => Object.keys(state.user).length === 0,
        transactions: state => state.transactions
    }
}