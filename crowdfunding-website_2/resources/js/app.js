window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

window.Vue = require('vue').default;
import App from './App.vue';
import router from './router.js';
import store from './vuex.js';

Vue.component('appbar-component', require('./components/AppbarComponent.vue').default);


const app = new Vue({
    el: '#app',
    store,
    router,
    data: {},
    components: {
        'app-component': App,
    },
    methods: {},

}).$mount('#app');
