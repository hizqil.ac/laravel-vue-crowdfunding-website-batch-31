import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
const Home = require('./views/Home.vue').default
const Campaign = require('./views/Campaign.vue').default
const CampaignSlug = require('./views/CampaignSlug.vue').default
const Login = require('./views/Login.vue').default
const Register = require('./views/Register.vue').default
const Verification = require('./views/Verification.vue').default
const UpdatePassword = require('./views/UpdatePassword.vue').default
const UpdateProfile = require('./views/UpdateProfile.vue').default
const Socialite = require('./views/Socialite.vue').default

const router = new VueRouter({
    linkActiveClass: 'active',
    mode: 'history',
    routes: [{
            path: '/',
            name: 'home',
            component: Home,
            props: true
        },
        {
            path: '/Campaign',
            name: 'campaign',
            component: Campaign
        },
        {
            path: '/Campaign/:slug?',
            component: CampaignSlug,
            props: true
        },
        {
            path: '/Login',
            name: 'login',
            component: Login,
            props: true
        },
        {
            path: '/Register',
            name: 'register',
            component: Register,
            props: true
        },
        {
            path: '/Verification',
            name: 'verification',
            component: Verification,
            props: true
        },
        {
            path: '/Update-Password',
            name: 'Update-Password',
            component: UpdatePassword,
            props: true
        },
        {
            path: '/Update-Profile',
            name: 'Update-Profile',
            component: UpdateProfile,
            props: true
        },
        {
            path: '/Auth/Sosialite/:provider/callback',
            name: 'socialite',
            component: Socialite,
        },
        {
            path: '*',
            redirect: '/'
        }
    ]
})


export default router