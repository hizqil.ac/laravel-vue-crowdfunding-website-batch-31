<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Resources\Profile\GetProfileResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    //get-profile user login
    public function show()
    {
        if (!auth()->user()) {
            return response()->json([
                'Response_code' => '01',
                'Response_message' => 'Harap Melakukan Login Terlebih dahulu',
            ]);
        }

        $auth = auth()->user();
        $user = User::find($auth->id);

        return response()->json([
            'Response_code' => '00',
            'Response_message' => 'Profile Berhasil Ditampilkan',
            'Data' => [
                'Profile' => new GetProfileResource($user)
            ],
        ]);

    }

    // update profile user login
    public function update(Request $request)
    {
        $request->validate([
            'name' => ['string'],
            'username' => ['unique:users,username']
        ]);

        if (!auth()->user()) {
            return response()->json([
                'Response_code' => '01',
                'Response_message' => 'Harap Melakukan Login Terlebih dahulu',
            ]);
        }

        $auth = auth()->user();
        $user = User::find($auth->id);
        $user->name = $request->name;
        $user->username = $request->username;
        $user->save();

        return response()->json([
            'Response_code' => '00',
            'Response_message' => 'Profile Berhasil diperbaharui',
            'Data' => [
                'Profile' => new GetProfileResource($user)
            ],
        ]);
    }


}
