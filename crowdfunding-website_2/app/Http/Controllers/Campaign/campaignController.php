<?php

namespace App\Http\Controllers\Campaign;

use App\Http\Controllers\Controller;
use App\Http\Resources\Campaign\GetcampaignResource;
use App\Models\campaign;
use Illuminate\Http\Request;

class campaignController extends Controller
{
    public function getCampaign()
    {
        $campaign = campaign::all();

        return response()->json([
            'Response_code' => '00',
            'Response_message' => 'Campaign Berhasil Ditampilkan',
            'Data' => [
                'campaign' => GetcampaignResource::collection($campaign)
            ],
        ]);
    }

    public function getCampaigncount($count)
    {
        $campaign = campaign::paginate($count);

        return response()->json([
            'Response_code' => '00',
            'Response_message' => 'Campaign Berhasil Ditampilkan',
            'Data' => [
                'campaign' => GetcampaignResource::collection($campaign)
            ],
        ]);
    }

    public function showCampaign($slug)
    {
        $campaign = campaign::where('slug' , $slug)->first();

        // dd($campaign);
        return response()->json([
            'Response_code' => '00',
            'Response_message' => 'Campaign Berhasil Ditampilkan',
            'campaign' => new GetcampaignResource($campaign)
        ]);
    }
}
