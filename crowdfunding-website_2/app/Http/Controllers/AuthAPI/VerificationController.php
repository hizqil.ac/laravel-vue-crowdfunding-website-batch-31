<?php

namespace App\Http\Controllers\AuthAPI;

use App\Http\Controllers\Controller;
use App\Http\Resources\AuthAPI\RegisterResource;
use App\Http\Resources\AuthAPI\VerificationResource;
use App\Models\otp_code;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request )
    {
        $request->validate([
            'otp' => ['required', 'numeric'],
        ]);

        $otp_code = otp_code::where('otp', $request->otp)->first();

        if ($otp_code == null ) {
            return response()->json([
                'Response_code' => '01',
                'Response_message' => 'OTP Tidak Ditemukan',
            ], 200);
        }

        $waktu_Sekarang = Carbon::now();
        if ($waktu_Sekarang > $otp_code->valid_until) {
            return response()->json([
                'Response_code' => '01',
                'Response_message' => 'Kode OTP Sudah tidak berlaku, Silahkan generate ulang',
            ], 200);
        }

        // Update User
        $user = User::find($otp_code->user_id);
        $user->email_verified_at =  Carbon::now();
        $user->save();

        // Delete Otp
        $otp_code->delete();

        return response()->json([
            'Response_code' => '00',
            'Response_message' => 'Verifikasi Berhasil, Cek Email Untuk update Password',
            'Data' => [
                'User' => new VerificationResource($user)
            ],
        ]);

    }
}
