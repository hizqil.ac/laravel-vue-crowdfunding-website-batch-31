<?php

namespace App\Http\Controllers\AuthAPI;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class GoogleSosialiteController extends Controller
{
    public function redirectToProvider($provider)
    {
        $url = Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();
        // ->stateless()
        return response()->json([
            'url' => $url
        ]);
    }

    public function handleProviderCallback($provider)
    {
        $sosial_user = Socialite::driver($provider)->stateless()->user();


        if (!$sosial_user) {
            return response()->json([
                'Response_code' => '01',
                'Response_Massage' => 'Login Failed',
            ]);
        }

        $user = User::Where('email', $sosial_user->email)->first();

        if (!$user) {
            $user = new User();
            $user->name = $sosial_user->name;
            $user->email = $sosial_user->email;
            $user->username = $sosial_user->id;
            $user->email_verified_at = Carbon::now();

            if ($provider == 'google') {
                $pp = $sosial_user->avatar;
                $user->photo = $pp;
            }
            $user->save();
        }

        $data['Token'] = auth()->login($user);
        $data['User'] = $user;

        return response()->json([
            'Response_code' => '00',
            'Response_message' => 'User Login Berhasil',
            'Data' => $data
        ], 200  );

    }
}
