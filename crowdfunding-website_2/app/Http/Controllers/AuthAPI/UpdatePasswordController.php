<?php

namespace App\Http\Controllers\AuthAPI;

use App\Http\Controllers\Controller;
use App\Http\Resources\AuthAPI\UpdatePasswordResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // $request->validate([
        //     'email' => ['required'. 'email'],
        //     'password' => ['required', 'confirmed']
        // ]);


        $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required', 'confirmed']
        ]);


        $user = User::where('email', request('email'))->first();

        if ($user->email_verified_at == null) {
            return response()->json([
                'Response_code' => '01',
                'Response_message' => 'Harap Melakukan Verifikasi Terlebih Dahulu',
            ], 200);
        }

        $user->password = Hash::make(request('password'));
        $user->save();

        return response()->json([
            'Response_code' => '00',
            'Response_Message' => 'Password Berhasil Diubah',
            'Data' => [
                'User' => new UpdatePasswordResource($user)
            ],
        ]);

    }
}
