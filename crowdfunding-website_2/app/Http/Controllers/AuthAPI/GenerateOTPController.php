<?php

namespace App\Http\Controllers\AuthAPI;

use App\Http\Controllers\Controller;
use App\Http\Resources\AuthAPI\RegisterResource;
use App\Http\Resources\AuthAPI\VerificationResource;
use App\Models\otp_code;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\UserAuthEvent;

class GenerateOTPController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' =>['required', 'email'],
        ]);

        $user = User::where('email', request('email'))->first();

        if ($user->email_verified_at != null) {
            return response()->json([
                'Response_code' => '01',
                'Response_message' => 'Email Sudah terverifikasi, tidak bisa genarate otp kembali',
            ],200);
        }

        $otp = otp_code::where('user_id', $user->id)->first();
        $otp->otp = random_int(100000, 999999);
        $otp->valid_until = Carbon::now()->addMinutes(15);
        $otp->save();

        UserAuthEvent::dispatch($user);


        return response()->json([
            'Response_code' => '00',
            'Response_Message' => 'Silahkan Cek Email',
            'Data' => [
                'User' => new RegisterResource($user)
            ],
        ]);



    }
}
