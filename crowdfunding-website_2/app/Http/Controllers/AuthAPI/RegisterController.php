<?php

namespace App\Http\Controllers\AuthAPI;

use App\Events\UserAuthEvent;
use App\Models\otp_code;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Resources\AuthAPI\RegisterResource;
use Illuminate\Http\Request;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'name' => ['required'],
            'username' =>['required', 'unique:users,username'] ,
            'email' =>['required', 'email', 'unique:users,email'] ,
        ]);

        $user = new User();
        $user->name = request('name');
        $user->username = request('username');
        $user->email = request('email');
        // if ($request->hasFile('photo')){
        //     $file = $request->file('gambar');
        //     $extension = $file->getClientOriginalExtension();
        //     $filename = 'Uploads/Photo/Users/' . $user->username . "." . $extension;
        //     $file->move('Uploads/Photo/Users', $filename);
        //     $user->photo = $filename;
        // }
        $user->save();

        $otp = new otp_code();
        $otp->otp = random_int(100000, 999999);
        $otp->valid_until = Carbon::now()->addMinutes(15);
        $otp->user_id = $user->id;
        $otp->save();

        UserAuthEvent::dispatch($user);

        return response()->json([
            'Response_code' => '00',
            'Response_Message' => 'Silahkan Cek Email',
            'Data' => [
                'User' => new RegisterResource($user)
            ],

        ]);
    }
}
