<?php

namespace App\Http\Controllers\AuthAPI;

use App\Http\Controllers\Controller;
use App\Http\Resources\AuthAPI\LoginResource;
use App\Models\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required']
        ]);



        if (! $token = auth()->attempt($request->only('email', 'password'))) {
            return response()->json([
                'error' => 'Harap Periksa Kembali Email dan Password'
            ], 401);
        }

        $user = User::where('email', request('email'))->first();
        return response()->json([
            'Response_code' => '00',
            'Response_message' => 'User Login Berhasil',
            'Data' => [
                'Token' => $token,
                'User' => new LoginResource($user)
            ],
        ]);
    }
}
