<?php

namespace App\Listeners;

use App\Events\UserAuthEvent;
use App\Mail\AuthAPI\UserRegister;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendNotificationUserRegister implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\UserAuthEvent  $event
     * @return void
     */
    public function handle(UserAuthEvent $event)
    {
        Mail::to($event->user)->send(new UserRegister($event->user));
    }
}
