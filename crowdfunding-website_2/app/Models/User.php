<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject
{
    // mengunakan laravel 8
    use HasApiTokens, HasFactory, Notifiable;

    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected $attributes = [
        'role_id' => '76020b6e-7a7c-4fd4-9789-8fe2868185a2',
    ];

    public function getRouteKeyName()
    {
        return 'username';
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

    // relasi one to many ke table Roles
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    // relasi one to many ke table otp_code
    public function otpCode(){
    	return $this->hasMany(otp_code::class);
    }


    protected $fillable = [
        'username',
        'name',
        'email',
        'password',
        'photo',
        'email_verified_at',
        'role_id',
    ];


    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function verifikasiEmail()
    {
        if($this->email_verified_at != null){
            return true;
        }
        return false;
    }

    public function aksesAdmin($admin)
    {
        // $aksesAdmin = Role::where('name', $adminname)->first()->id;
        // return true;
        if($admin == "admin"){
            return true;
        }
        return false;
    }


    // JWT
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
