<?php

use App\Http\Controllers\AuthAPI\CheckTokenController;
use App\Http\Controllers\AuthAPI\GenerateOTPController;
use App\Http\Controllers\AuthAPI\GoogleSosialiteController;
use App\Http\Controllers\AuthAPI\LoginController;
use App\Http\Controllers\AuthAPI\LogoutController;
use App\Http\Controllers\AuthAPI\registerController;
use App\Http\Controllers\AuthAPI\UpdatePasswordController;
use App\Http\Controllers\AuthAPI\VerificationController;
use App\Http\Controllers\Campaign\campaignController;
use App\Http\Controllers\Profile\ProfileController;
use Illuminate\Support\Facades\Route;

Route::prefix('/Auth')->group(function () {
    Route::post('/Register', registerController::class);
    Route::post('/Verification', VerificationController::class);
    Route::post('/Generate-OTP', GenerateOTPController::class);
    Route::post('/Update-Password', UpdatePasswordController::class);
    Route::post('/Login', LoginController::class);
    Route::post('/Logout', LogoutController::class)->middleware('auth:api');
    Route::post('/Check-Token', CheckTokenController::class)->middleware('auth:api');

    Route::get('/Sosialite/{provider}', [GoogleSosialiteController::class, 'redirectToProvider']);
    Route::get('/Sosialite/{provider}/callback', [GoogleSosialiteController::class, 'handleProviderCallback']);

});


Route::prefix('/Profile')->group(function () {
    Route::get('/get-profile', [ProfileController::class, 'show']);
    Route::post('/update-profile', [ProfileController::class, 'update']);
});

Route::prefix('/Campaign')->group(function () {
    Route::get('/get-campaign', [campaignController::class, 'getCampaign']);
    Route::get('/get-campaign/{count}', [campaignController::class, 'getCampaigncount']);
    Route::get('/show-campaign/{id}', [campaignController::class, 'showCampaign']);
});
