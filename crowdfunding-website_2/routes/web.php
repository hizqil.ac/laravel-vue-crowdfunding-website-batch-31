<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/rumah', function () {
    return view('app');
});

Route::get('/{any?}', function () {
    return view('welcome');
})->where('any', '.*');


Route::get('/route-1', function () {
    return '/route-1 yang diproteksi menggunakan middleware verifikasi email';
})->middleware('verifikasiEmail');

Route::get('/route-2', function () {
    return 'route-2 yang diproteksi menggunakan middleware verifikasi email dan middleware admin';
})->middleware(['verifikasiEmail', 'aksesAdmin']);

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
