<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name' => 'hizqil',
            'username' => 'hizqil',
            'email' =>'hizqil@gmail.com',
            'password' => Hash::make('hizqil123'),
            'role_id' =>'73e69ff3-5c2a-4a3d-9768-21cf864f5cfa',
        ]);
    }
}
