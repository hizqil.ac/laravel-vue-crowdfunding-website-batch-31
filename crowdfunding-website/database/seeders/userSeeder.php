<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id' => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name' => 'hizqil',
            'username' => 'hizqil',
            'email' => 'hizqil@gmail.com',
            'password' => Hash::make('hizqil123'),
        ]);
    }
}
