<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    // mengunakan laravel 8

    use HasApiTokens, HasFactory, Notifiable;

    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

    // relasi one to many ke table Roles
    // public function Role()
    // {
    //     return $this->belongsTo(Role::class);
    // }


    protected $fillable = [
        'username',
        'name',
        'email',
        'role_id',
    ];


    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function aksesUser()
    {
        if($this->role == "user"){
            return true;
        }
        return false;
    }

    public function aksesAdmin()
    {
        if($this->role == "admin"){
            return true;
        }
        return false;
    }

    public function verifikasiEmail()
    {
        if($this->email_verified_at != null){
            return true;
        }
        return false;
    }

}
