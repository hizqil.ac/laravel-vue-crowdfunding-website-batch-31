<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class tugas3Controller extends Controller
{
    public function index()
    {
        return '/route-1 ini yang diproteksi menggunakan middleware verifikasi email (poin 1)';
    }
    public function route_2()
    {
        return 'route-2 yang diproteksi menggunakan middleware verifikasi email (poin 1) dan middleware admin (poin 2)';
    }
}
