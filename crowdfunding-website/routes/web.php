<?php

use App\Http\Controllers\tugas3Controller;
use Illuminate\Foundation\Application;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

//laravel breeze vue

// Route::get('/', function () {
//     return Inertia::render('Welcome', [
//         'canLogin' => Route::has('login'),
//         'canRegister' => Route::has('register'),
//         'laravelVersion' => Application::VERSION,
//         'phpVersion' => PHP_VERSION,
//     ]);
// });

Route::get('/dashboard', function () {
    return view('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');


Route::get('/', function(){
    return view('welcome');
});


Route::get('/route-1', [tugas3Controller::class, 'index'])->middleware('verifikasiEmail');
Route::get('/route-2', [tugas3Controller::class, 'route_2'])->middleware(['verifikasiEmail', 'aksesAdmin']);


require __DIR__.'/auth.php';
