// soal no 1
const PersegiPanjang = (Panjang, Lebar) => {
    let luas = Panjang * Lebar;
    let keliling = 2 * (Panjang + Lebar);
    return "Luas = " + luas + "\n" +
        "Keliling = " + keliling;
}
console.log(PersegiPanjang(2, 3));




// soal no 2
const Mahasiswa = (firstName, lastName) => {
    let fullname = `${firstName} ${lastName}`;
    return fullname;
}
console.log(Mahasiswa('Machsalmina', 'Hizqil'));




//soal no 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
const { firstName, lastName, address, hobby } = newObject
console.log(firstName, lastName, address, hobby)




//soal no 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined)




//soal no 5
const planet = "earth"
const view = "glass"
let before = `Lorem ${planet} dolor sit amet, consectetur adipiscing elit ${view}`
console.log(before);